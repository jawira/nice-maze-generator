<?php declare(strict_types=1);

namespace Jawira\NiceMazeGenerator;

use Exception;

class MazeException extends Exception
{
}
