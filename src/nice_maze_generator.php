<?php declare(strict_types=1);

/**
 * @author  Jawira Portugal <dev@tugal.be>
 * @license MIT
 */

namespace Jawira\NiceMazeGenerator;

use function array_pop;
use function array_push;
use function compact;
use function end;
use function is_array;
use function range;
use function shuffle;
use const PHP_EOL;

const N        = -1;
const E        = 1;
const S        = 1;
const W        = -1;
const N_FLAG   = 8;
const E_FLAG   = 4;
const S_FLAG   = 2;
const W_FLAG   = 1;
const NO_WALL  = 0;
const MIN_SIZE = 2;
const COL      = 'col';
const ROW      = 'row';
const SPACE    = ' ';
const ·        = '·';
const ─        = '─';
const │        = '│';
const ├        = '├';
const ┤        = '┤';
const ┬        = '┬';
const ┴        = '┴';
const ┼        = '┼';
const ╭        = '╭';
const ╮        = '╮';
const ╯        = '╯';
const ╰        = '╰';
const ╴        = '╴';
const ╵        = '╵';
const ╶        = '╶';
const ╷        = '╷';

/**
 * Create skeleton grid.
 */
function create_grid(int $rows, int $cols): array
{
  $builder = function (string|array $content, string|array $glue, string|array $left, string|array $right, int $count) {
    $line = array_map(fn(int $value) => $value % 2 === 0 ? $content : $glue, range(2, $count * 2));
    array_unshift($line, $left);
    array_push($line, $right);

    return $line;
  };

  return $builder($builder(·, │, │, │, $cols),
                  $builder(─, ┼, ├, ┤, $cols),
                  $builder(─, ┬, ╭, ╮, $cols),
                  $builder(─, ┴, ╰, ╯, $cols),
                  $rows,);
}

/**
 * Returns unvisited neighbors.
 */
function neighbors(array &$grid, array $position): array
{
  $neighbors   = [];
  $addNeighbor = function (int $row, int $col) use (&$grid, &$neighbors) {
    if (isset($grid[$row][$col]) && $grid[$row][$col] === ·) {
      array_push($neighbors, compact(ROW, COL));
    }
  };
  $addNeighbor($position[ROW], $position[COL] + E + E);
  $addNeighbor($position[ROW], $position[COL] + W + W);
  $addNeighbor($position[ROW] + N + N, $position[COL]);
  $addNeighbor($position[ROW] + S + S, $position[COL]);

  return $neighbors;
}

/**
 * Returns one random unvisited neighbor, and null if there's no unvisited neighbor.
 */
function random_neighbor(array &$grid, array $position): ?array
{
  $neighbors = neighbors($grid, $position);
  shuffle($neighbors);

  return array_pop($neighbors);
}

/**
 * Overwrite grid cells with white space.
 */
function paint_cells(array &$grid, array $from, array $to): void
{
  foreach (range($from[ROW], $to[ROW]) as ${ROW}) {
    foreach (range($from[COL], $to[COL]) as ${COL}) {
      $grid[${ROW}][${COL}] = SPACE;
    }
  }
}

/**
 * Generates a maze.
 *
 * @param int $rows Number of horizontal corridors, must be greater or equal than 2.
 * @param int $cols Number of vertical corridors, must be greater or equal than 2.
 * @return array The maze
 * @throws MazeException
 */
function maze(int $rows, int $cols): array
{
  if ($rows < MIN_SIZE) throw new MazeException('Rows must be greater or equal than ' . MIN_SIZE);
  if ($cols < MIN_SIZE) throw new MazeException('Cols must be greater or equal than ' . MIN_SIZE);
  $grid  = create_grid($rows, $cols);
  $stack = [[ROW => S, COL => array_key_last(reset($grid)) + W]];
  do {
    $current = end($stack);
    $next    = random_neighbor($grid, $current);
    if (is_array($next)) {
      paint_cells($grid, $current, $next);
      array_push($stack, $next);
    } else {
      array_pop($stack);
    }
  } while (!empty($stack));
  beautify($grid, array_key_last($grid), array_key_last(end($grid)));

  return $grid;
}

/**
 * String version of 2d array.
 */
function maze_to_string(array $maze): string
{
  return array_reduce($maze, fn($carry, $item) => $carry . implode('', $item) . PHP_EOL, '');
}

/**
 * Prunes maze's ugly borders.
 */
function beautify(array &$grid, int $rows, int $cols): void
{
  $equivalencies = [SPACE, ╴, ╷, ╮, ╶, ─, ╭, ┬, ╵, ╯, │, ┤, ╰, ┴, ├, ┼,];
  $hasWall       = fn(int $row, int $col, $flag) => $grid[$row][$col] === SPACE ? NO_WALL : $flag;
  for (${ROW} = 0; ${ROW} <= $rows; ${ROW}++) {
    for (${COL} = 0; ${COL} <= $cols; ${COL}++) {
      switch ($grid[${ROW}][${COL}]) {
        case ┬:
          if ($grid[${ROW} + S][${COL}] === SPACE) $grid[${ROW}][${COL}] = ─;
          break;
        case ┴:
          if ($grid[${ROW} + N][${COL}] === SPACE) $grid[${ROW}][${COL}] = ─;
          break;
        case ┤:
          if ($grid[${ROW}][${COL} + W] === SPACE) $grid[${ROW}][${COL}] = │;
          break;
        case ├:
          if ($grid[${ROW}][${COL} + E] === SPACE) $grid[${ROW}][${COL}] = │;
          break;
      }
    }
  }
  for (${ROW} = 0; ${ROW} <= $rows; ${ROW}++) {
    for (${COL} = 0; ${COL} <= $cols; ${COL}++) {
      if ($grid[${ROW}][${COL}] !== ┼) continue;
      $grid[${ROW}][${COL}] = $equivalencies[$hasWall(${ROW}, ${COL} + E, E_FLAG) | $hasWall(${ROW}, ${COL} + W, W_FLAG) | $hasWall(${ROW} + N, ${COL}, N_FLAG) | $hasWall(${ROW} + S, ${COL}, S_FLAG)];
    }
  }
}
