Nice Maze Generator
===================

**Create a maze using ascii art.**

[![Latest Stable Version](https://poser.pugx.org/jawira/nice-maze-generator/v)](//packagist.org/packages/jawira/nice-maze-generator)
[![.gitattributes](https://poser.pugx.org/jawira/nice-maze-generator/gitattributes)](//packagist.org/packages/jawira/nice-maze-generator)
[![composer.lock](https://poser.pugx.org/jawira/nice-maze-generator/composerlock)](//packagist.org/packages/jawira/nice-maze-generator)
[![License](https://poser.pugx.org/jawira/nice-maze-generator/license)](//packagist.org/packages/jawira/nice-maze-generator)

Usage
-----

Use `maze()` function to create an _array_ containing the maze.

```php
<?php
// demo.php
require __DIR__ . '/vendor/autoload.php';

use function Jawira\NiceMazeGenerator\maze;
use function Jawira\NiceMazeGenerator\maze_to_string;

$arrayMaze  = maze(5, 20); // 5 rows, 20 columns
$stringMaze = maze_to_string($arrayMaze);

echo $stringMaze;
```

This is the output from previous code:

```console
╭─────────────┬───────┬───┬───┬───────┬─╮
│             │       │   │   │       │ │
│ ╭─────┬─╴ ╷ ├─╴ ╭─╴ │ ╷ │ ╷ │ ╶─╮ ╷ │ │
│ │     │   │ │   │   │ │ │ │ │   │ │ │ │
│ │ ╭───╯ ╭─╯ │ ╭─┴─╮ ╵ │ ╵ │ ╰─╮ │ ╰─╯ │
│ │ │     │   │ │   │   │   │   │ │     │
│ ╵ │ ╶───┴─╮ ╵ │ ╷ ├───┴───┴─┬─╯ ├───┬─┤
│   │       │   │ │ │         │   │   │ │
├─╴ ╰─────╮ ╰───╯ │ ╵ ╶─────╮ ╵ ╶─╯ ╷ ╵ │
│         │       │         │       │   │
╰─────────┴───────┴─────────┴───────┴───╯
```

How to install
--------------

```console
$ composer require jawira/nice-maze-generator
```

Contributing
------------

- If you liked this project, ⭐ star it on [GitLab](https://gitlab.com/jawira/nice-maze-generator).
- Issues are welcomed.

License
-------

This library is licensed under the [MIT license](LICENSE.md).
