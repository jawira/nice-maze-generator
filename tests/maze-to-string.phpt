--TEST--
String version of a maze
--FILE--
<?php
require __DIR__ . DIRECTORY_SEPARATOR. '../vendor/autoload.php';

use function \Jawira\NiceMazeGenerator\maze;
use function \Jawira\NiceMazeGenerator\maze_to_string;

$maze = maze(5,13);
$str = maze_to_string($maze);
echo gettype($str),PHP_EOL, \mb_strlen($str);
--EXPECT--
string
308
