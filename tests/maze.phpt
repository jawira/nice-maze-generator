--TEST--
Basic usage
--FILE--
<?php
require __DIR__ . DIRECTORY_SEPARATOR. '../vendor/autoload.php';

use function \Jawira\NiceMazeGenerator\maze;

$maze = maze(5,13);
echo gettype($maze), PHP_EOL, count($maze), PHP_EOL, count(reset($maze));
--EXPECT--
array
11
27
