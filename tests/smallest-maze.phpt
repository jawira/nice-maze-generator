--TEST--
Smallest maze
--FILE--
<?php
require __DIR__ . DIRECTORY_SEPARATOR. '../vendor/autoload.php';

use function \Jawira\NiceMazeGenerator\maze;

$maze = maze(2, 2);
echo count($maze), PHP_EOL, count(reset($maze));
--EXPECT--
5
5
