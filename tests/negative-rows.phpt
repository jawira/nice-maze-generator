--TEST--
Rectify minimum size
--FILE--
<?php
require __DIR__ . DIRECTORY_SEPARATOR. '../vendor/autoload.php';

use \Jawira\NiceMazeGenerator\MazeException;
use function \Jawira\NiceMazeGenerator\maze;

try{
  $maze = maze(-2, 10);
} catch (MazeException $exception){
  echo $exception->getMessage();
}
--EXPECT--
Rows must be greater or equal than 2
