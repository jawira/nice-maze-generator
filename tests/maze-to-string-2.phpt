--TEST--
Flatten an array
--FILE--
<?php
require __DIR__ . DIRECTORY_SEPARATOR. '../vendor/autoload.php';

use function \Jawira\NiceMazeGenerator\maze_to_string;

$demo = [['a','b'],['c','d']];
echo maze_to_string($demo);
--EXPECT--
ab
cd
